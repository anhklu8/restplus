import random


def generate_random_number(digit):
    num = random.randrange(1, 10**(int(digit)))
    num_with_zeros = '{:04}'.format(num)
    return num_with_zeros
