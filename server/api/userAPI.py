from flask_restplus import Namespace, Resource, fields
from flask_restplus._http import HTTPStatus
from db.orm.SQLAlchemy import alchemist
from db.models.userModel import UserModel
from db.schema.marshmallow import UserSchema
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity,
                                create_access_token,
                                create_refresh_token,
                                jwt_optional,
                                jwt_refresh_token_required)
from server.utils.friendlyID import generate_random_number
from werkzeug.security import generate_password_hash, check_password_hash, safe_str_cmp
import bcrypt

ns_user = Namespace('api', description='CRUD API for User Data')

parser = ns_user.parser()
parser.add_argument('userEmail', type=str)
parser.add_argument('userPassword', type=str)
parser.add_argument('firstName', type=str)
parser.add_argument('lastName', type=str)

user_model = ns_user.model(
    'User',
    {
        'firstName': fields.String('First Name'),
        'lastName': fields.String('Last Name'),
        'userEmail': fields.String('Email Address'),
        'userPassword': fields.String('Password'),
    }
)


@ns_user.route('/user')
class UserList(Resource):
    @ns_user.doc('list_of_registered_users', security='apiKey')
    @ns_user.response(200, 'Success', user_model)
    # @jwt_required
    def get(self):
        all_users = UserModel.query.all()
        total_count = UserModel.query.count()
        data_schema = UserSchema(many=True)
        output = data_schema.dump(all_users).data
        return {'data': output, 'total': total_count}

    @ns_user.doc('create_user')
    @ns_user.expect(user_model)
    @ns_user.response(201, 'User added')
    # @jwt_required
    def post(self):
        password = ns_user.payload['userPassword'].encode('utf-8')
        new_user = UserModel(
            friendlyID='hm{}'.format(generate_random_number(4)),
            firstName=ns_user.payload['firstName'],
            lastName=ns_user.payload['lastName'],
            fullName=ns_user.payload['firstName']+' '+ns_user.payload['lastName'],
            userEmail=ns_user.payload['userEmail'],
            hashedPwd=bcrypt.hashpw(password, bcrypt.gensalt(rounds=14))
        )
        alchemist.session.add(new_user)
        alchemist.session.commit()
        return {'result': 'user added'}


@ns_user.route('/user/<string:friendlyID>')
@ns_user.response(
    code=HTTPStatus.NOT_FOUND,
    description='User not found.',
)
class User(Resource):
    @ns_user.response(200, 'Success')
    @ns_user.doc('get a user')
    # @jwt_required
    def get(self, friendlyID):
        # current_user = get_jwt_identity()
        # if current_user:
        a_user = UserModel.query.filter_by(
            friendlyID=friendlyID).first_or_404()
        total_count = UserModel.query.filter_by(
            friendlyID=friendlyID).count()
        data_schema = UserSchema(many=False)
        output = data_schema.dump(a_user).data
        return {'data': output, 'total': total_count}
        # return {'msg': 'Need to log in'}

    @ns_user.doc('update user details by friendlyID.')
    @ns_user.response(204, 'User updated')
    @ns_user.expect(parser)
    # @jwt_required
    def put(self, friendlyID):
        user_email = parser.parse_args().userEmail
        user_password = parser.parse_args().userPassword
        user_firstname = parser.parse_args().firstName
        user_lastname = parser.parse_args().lastName
        # current_user = get_jwt_identity()
        # if current_user:
        a_user_to_be_updated = UserModel.query.filter_by(
            friendlyID=friendlyID).first_or_404()
        a_user_to_be_updated.firstName = user_firstname
        a_user_to_be_updated.lastName = user_lastname
        a_user_to_be_updated.userEmail = user_email
        a_user_to_be_updated.userPassword = user_password
        alchemist.session.commit()
        return {'result': 'user updated'}
        # return {'msg': 'Need to log in'}

    @ns_user.doc('delete a user')
    @ns_user.response(204, 'User deleted')
    # @jwt_required
    def delete(self, friendlyID):
        UserModel.query.filter_by(friendlyID=friendlyID).delete()
        alchemist.session.commit()
        return '', 204
