from flask_restplus import Namespace, Resource, fields
from flask_restplus._http import HTTPStatus
from db.orm.SQLAlchemy import alchemist
from db.models.timesheetModel import TimesheetModel
from db.schema.marshmallow import TimesheetSchema
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity,
                                create_access_token,
                                create_refresh_token,
                                jwt_optional,
                                jwt_refresh_token_required)
from server.utils.friendlyID import generate_random_number
from werkzeug.security import generate_password_hash, check_password_hash, safe_str_cmp
import bcrypt

ns_timesheet = Namespace(
    'timesheet', description='CRUD API for Timesheet Data')

parser = ns_timesheet.parser()
parser.add_argument('userEmail', type=str)
parser.add_argument('userPassword', type=str)

timesheet_model = ns_timesheet.model(
    'Timesheet',
    {
        'date': fields.Date(),
        'startTime': fields.DateTime('Start time'),
        'endTime': fields.DateTime('End time'),
    }
)


@ns_timesheet.route('/timesheet')
class TimesheetList(Resource):
    @ns_timesheet.doc('list_of_working_hours', security='apiKey')
    @ns_timesheet.response(200, 'Success', timesheet_model)
    # @jwt_required
    def get(self):
        all_timesheet = TimesheetModel.query.all()
        total_count = TimesheetModel.query.count()
        data_schema = TimesheetSchema(many=True)
        output = data_schema.dump(all_timesheet).data
        return {'data': output, 'total': total_count}

    @ns_timesheet.doc('create_new_time_sheet')
    @ns_timesheet.expect(timesheet_model)
    @ns_timesheet.response(201, 'timesheet added')
    # @jwt_required
    def post(self):
        new_timesheet = TimesheetModel(
            friendlyID='hm{}'.format(generate_random_number(4)),
            date=ns_timesheet.payload['date'],
            startTime=ns_timesheet.payload['startTime'],
            endTime=ns_timesheet.payload['endTime']
        )
        alchemist.session.add(new_timesheet)
        alchemist.session.commit()
        return {'result': 'time added'}


# @ns_timesheet.route('/user/<string:friendlyID>')
# @ns_timesheet.response(
#     code=HTTPStatus.NOT_FOUND,
#     description='User not found.',
# )
# class User(Resource):
#     @ns_timesheet.response(200, 'Success')
#     @ns_timesheet.doc('get a user')
#     @jwt_required
#     def get(self, friendlyID):
#         current_user = get_jwt_identity()
#         if current_user:
#             a_user = TimesheetModel.query.filter_by(
#                 friendlyID=friendlyID).first_or_404()
#             total_count = TimesheetModel.query.filter_by(
#                 friendlyID=friendlyID).count()
#             data_schema = TimesheetSchema(many=False)
#             output = data_schema.dump(a_user).data
#             return {'data': output, 'total': total_count}
#         return {'msg': 'Need to log in'}
#     @ns_timesheet.doc('update user details by friendlyID.')
#     @ns_timesheet.response(204, 'User updated')
#     @ns_timesheet.expect(timesheet_model)
#     @jwt_required
#     def put(self, friendlyID):
#         current_user = get_jwt_identity()
#         if current_user:
#             a_user_to_be_updated = TimesheetModel.query.filter_by(
#                 friendlyID=friendlyID).first_or_404()
#             a_user_to_be_updated.firstName = ns_timesheet.payload['firstName']
#             a_user_to_be_updated.lastName = ns_timesheet.payload['lastName']
#             a_user_to_be_updated.userEmail = ns_timesheet.payload['userEmail']
#             a_user_to_be_updated.userPassword = ns_timesheet.payload['userPassword']
#             alchemist.session.commit()
#             return {'result': 'user updated'}
#         return {'msg': 'Need to log in'}
#     @ns_timesheet.doc('delete a user')
#     @ns_timesheet.response(204, 'User deleted')
#     @jwt_required
#     def delete(self, friendlyID):
#         TimesheetModel.query.filter_by(friendlyID=friendlyID).delete()
#         alchemist.session.commit()
#         return '', 204
