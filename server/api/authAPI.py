from flask_restplus import Namespace, Resource, fields
from flask_restplus._http import HTTPStatus
from db.orm.SQLAlchemy import alchemist
from db.models.userModel import UserModel
from marshmallow_sqlalchemy import ModelSchema
from db.schema.marshmallow import UserSchema
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity,
                                create_access_token,
                                create_refresh_token,
                                jwt_optional,
                                jwt_refresh_token_required)
from server.utils.friendlyID import generate_random_number
from werkzeug.security import generate_password_hash, check_password_hash, safe_str_cmp
import bcrypt

ns_auth = Namespace(
    'auth', description='API for authentication and authorization')

parser = ns_auth.parser()
parser.add_argument('userEmail', type=str)
parser.add_argument('userPassword', type=str)

user_model = ns_auth.model(
    'User',
    {
        'firstName': fields.String('First Name'),
        'lastName': fields.String('Last Name'),
        'userEmail': fields.String('Email Address'),
        'userPassword': fields.String('Password'),
    }
)


@ns_auth.route('/register')
class UserRegister(Resource):
    @ns_auth.doc('register a user')
    @ns_auth.expect(user_model)
    @ns_auth.response(201, 'User added')
    def post(self):
        password = ns_auth.payload['userPassword'].encode('utf-8')
        new_user = UserModel(
            friendlyID='hm{}'.format(generate_random_number(4)),
            firstName=ns_auth.payload['firstName'],
            lastName=ns_auth.payload['lastName'],
            fullName=ns_auth.payload['firstName'] +
            ' '+ns_auth.payload['lastName'],
            userEmail=ns_auth.payload['userEmail'],
            hashedPwd=bcrypt.hashpw(password, bcrypt.gensalt(rounds=12)),
        )
        alchemist.session.add(new_user)
        alchemist.session.commit()
        return {'result': 'user has been registered'}


@ns_auth.route('/login')
class UserLogin(Resource):
    @ns_auth.doc('create_login')
    @ns_auth.expect(parser)
    def post(self):
        user_email = parser.parse_args().userEmail
        user_password = parser.parse_args().userPassword
        # get data from the request
        # find user in database
        user = UserModel.query.filter_by(
            userEmail=user_email).first_or_404()
        data_schema = UserSchema(many=False)
        output = data_schema.dump(user).data
        # check password
        if user and bcrypt.checkpw(user_password.encode('utf8'), output['hashedPwd'].encode('utf-8')):
            # if user and check_password_hash(output.userPassword, userPassword):
            # if user and safe_str_cmp(output.password, userPassword):
            # create an access token
            access_token = create_access_token(
                identity=output['friendlyID'], fresh=True)
            # create a refresh token
            refresh_token = create_refresh_token(output['friendlyID'])
            return {
                'access_token': access_token,
                'refresh_token': refresh_token
            }, 200
        # return them to the client
        return {'msg': 'Invalid credentials'}, 401


@ns_auth.route('/refreshtoken')
class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        """
        Get a new access token without requiring username and password (only the 'refresh token'
        provided in the /login endpoint.)
        Note that refreshed access tokens have a `fresh=False`, which means that the user may have not
        given us their username and password for potentially a long time (if the token has been
        refreshed many times over).
        """
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {'access_token': new_token}, 200
