#### \* FLASK

1.  Install virtual environment:

        python3 -m venv venv

1.  Activate virtual environment:

        source myvenv/bin/activate

1.  Install these dependencies/packages:

- boto3
- flask
- Flask-Cors
- flask-marshmallow
- flask-restplus
- Flask-SQLAlchemy
- marshmallow
- marshmallow-sqlalchemy
- psycopg2
- SQLAlchemy

        pip install boto3 flask Flask-Cors flask-marshmallow flask-restplus Flask-SQLAlchemy marshmallow marshmallow-sqlalchemy psycopg2 SQLAlchemy flask_jwt_extended

- If you can't install `psycopg2`, run this command:

        env LDFLAGS="-I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib" pip --no-cache install psycopg2

#### \* POSTGRES

File: `dbConfig.ini`

    [postgresql]
    host=REPLACEME
    database=REPLACEME
    user=REPLACEME
    password=REPLACEME
    port=5432
    sslmode=require
