from db.models.userModel import UserModel
from db.models.timesheetModel import TimesheetModel
from marshmallow_sqlalchemy import ModelSchema


class UserSchema(ModelSchema):
    class Meta:
        model = UserModel


class TimesheetSchema(ModelSchema):
    class Meta:
        model = TimesheetModel
