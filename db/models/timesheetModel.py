from db.orm.SQLAlchemy import alchemist
from datetime import datetime


class TimesheetModel(alchemist.Model):  # This class is for SQLAlchemy alchemist
    __tablename__ = "timesheet"
    id = alchemist.Column(alchemist.Integer, primary_key=True)
    friendlyID = alchemist.Column(alchemist.String(
        20), unique=True, nullable=False)
    date = alchemist.Column(alchemist.DateTime(timezone=True), nullable=False,
                            default=datetime.utcnow)
    startTime = alchemist.Column(
        alchemist.DateTime(timezone=True), nullable=False)
    endTime = alchemist.Column(
        alchemist.DateTime(timezone=True), nullable=False)

    def __repr__(self):
        return "('user_id': {})".format(self.id)
