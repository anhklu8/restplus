from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship, backref
from db.orm.SQLAlchemy import alchemist


class UserModel(alchemist.Model):  # This class is for SQLAlchemy alchemist
    __tablename__ = "user"
    id = alchemist.Column(alchemist.Integer, primary_key=True)
    friendlyID = alchemist.Column(alchemist.String(
        20), unique=True, nullable=False)
    fullName = alchemist.Column(alchemist.String(
        50), unique=True, nullable=False)
    firstName = alchemist.Column(alchemist.String(20), nullable=False)
    lastName = alchemist.Column(alchemist.String(20), nullable=False)
    userEmail = alchemist.Column(
        alchemist.String(50), unique=True, nullable=False)
    hashedPwd = alchemist.Column(alchemist.LargeBinary(100), nullable=False)
    person = relationship("Person", uselist=False,
                          back_populates='user', single_parent=True)

    def __repr__(self):
        return "('user_id': {})".format(self.id)


class PersonModel(alchemist.Model):
    __tablename__ = 'person'
    id = alchemist.Column(alchemist.Integer, primary_key=True)
    user_id = alchemist.Column(
        alchemist.Integer, alchemist.ForeignKey('user.id'), nullable=False)
    first_name = alchemist.Column(alchemist.String(120), nullable=False)
    middle_name = alchemist.Column(alchemist.String(120), nullable=True)
    last_name = alchemist.Column(alchemist.String(120), nullable=False)
    user = relationship("User", uselist=False, back_populates='person')
