from db.orm.SQLAlchemy import alchemist


class TodoModel(alchemist.Model):  # This class is for SQLAlchemy alchemist
    __tablename__ = "todo"
    id = alchemist.Column(alchemist.Integer, primary_key=True)
    friendlyID = alchemist.Column(alchemist.String(
        20), unique=True, nullable=False)
    date = alchemist.Column(alchemist.DateTime(timezone=True), nullable=False,
                            default=datetime.utcnow)
    task = alchemist.Column(alchemist.String(
        20), nullable=False)
    complete = alchemist.Column(alchemist.Boolean, nullable=False)
    def __repr__(self):
        return "('user_id': {})".format(self.id)
