import csv
import json
import sys
# Open the CSV
f = open(sys.argv[1], 'rU')
# Change each fieldname to the appropriate field name. I know, so difficult.
reader = csv.DictReader(f, fieldnames=("userID", "fullName", "userFName", "userLName",
                                       "userEmail", "userPassword", "userDOB", "UserSSN", "userTel", "userGender", "userJobTitle"))
# Parse the CSV into JSON
out = json.dumps([row for row in reader])
print("JSON parsed!")
# Save the JSON
f = open(sys.argv[2], 'w')
f.write(out)
print("JSON saved!")
