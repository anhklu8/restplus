# Imports from libraries
from flask import Blueprint
from flask_restplus import Resource, Api, fields, apidoc

from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager


# Imports from project
from server import app
from server import CORS
from db.orm.SQLAlchemy import alchemist, get_postgres_url
from server.api import userAPI, authAPI, timesheetAPI


app.config['SQLALCHEMY_DATABASE_URI'] = get_postgres_url()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = 'super-secret'  # TODO: hide this key
# This allows Flask to see exceptions when JWT returns an error
app.config['PROPAGATE_EXCEPTIONS'] = True

authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token"
    }
}

user_rest_api_blueprint = Blueprint('myapp', __name__)
user_rest_api = Api(user_rest_api_blueprint, doc='/swagger/',
                    title='My API', authorizations=authorizations, security='apiKey')


user_rest_api.add_namespace(userAPI.ns_user)
user_rest_api.add_namespace(authAPI.ns_auth)
user_rest_api.add_namespace(timesheetAPI.ns_timesheet)
app.register_blueprint(user_rest_api_blueprint)

alchemist.init_app(app)
alchemist.create_all(app=app)
JWTManager(app)
CORS(app, supports_credentials=True)

if __name__ == '__main__':
    app.run(debug=True, port=8080)
# http://127.0.0.1:8080/swagger/

# from flask_jwt import JWT, jwt_required #then we can use @jwt_required in our calls
# from server.utils.jsonWebToken import authenticate, identity
# jwt = JWT(app, authenticate, identity) # jwt will create a new endpoint /auth, then  this endpoint will return JWT token

# Bearer token#
